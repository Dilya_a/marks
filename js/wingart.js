$(function(){
	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
	ua = navigator.userAgent,

	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

	scaleFix = function () {
		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
			document.addEventListener("gesturestart", gestureStart, false);
		}
	};
	
	scaleFix();
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')



// INCLUDE FUNCTION
	var tabs 	      = $("#parentHorizontalTab");

	
	if(tabs.length){
	  	include("js/easyResponsiveTabs.js");
	}


function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}




$(document).ready(function(){

	/* ------------------------------------------------
	TABS START
	------------------------------------------------ */

		if(tabs.length){
			tabs.easyResponsiveTabs({
				type: 'horizontal', 
		        width: 'auto', 
		        fit: true, 
		        closed: 'accordion', 
		        tabidentify: 'hor_1', 
			});
		}

	/* ------------------------------------------------
	TABS END
	------------------------------------------------ */


})